//
//  SNRStateManager.h
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/24/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SNRUserDefaultsStore.h"

@interface SNRStateManager : NSObject

//@property (nonatomic, strong) SNRUserDefaultsStore *dataStore;

- (void)addNewWeatherData:(NSDictionary *)weatherData;

@end
