//
//  SNRStateManager.m
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/24/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import "SNRStateManager.h"

@implementation SNRStateManager

- (void)addNewWeatherData:(NSDictionary *)weatherData
{
    [SNRUserDefaultsStore setWeatherData:weatherData];
}

@end
