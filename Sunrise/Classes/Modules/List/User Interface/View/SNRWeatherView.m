//
//  SNRWeatherView.m
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/22/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import "SNRWeatherView.h"
#import "H3DottedLine.h"
#import "Climacons.h"

#pragma mark - SNRWeatherView Implementation

@implementation SNRWeatherView

- (instancetype)initWithFrame:(CGRect)frame
{
    NSLog(@"initWithFrame");
    if(self = [super initWithFrame:frame]) {

        UIView *view = [[UIView alloc] initWithFrame:frame];

        //initialize ConditionIconLabel
        const NSInteger fontSize = 45;
        self.conditionIconLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 45, 45)];
        [self.conditionIconLabel setFont:[UIFont fontWithName:CLIMACONS_FONT size:fontSize]];
        [self.conditionIconLabel setBackgroundColor:[UIColor clearColor]];
        [self.conditionIconLabel setTextColor:[UIColor whiteColor]];
        [self.conditionIconLabel setTextAlignment:NSTextAlignmentCenter];
        [view addSubview:self.conditionIconLabel];
        /*
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, 25, 25)];
        [imageView setImage:[UIImage imageNamed:@"sunny"]];
        [view addSubview:imageView];
         */

        self.weatherDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(55, self.conditionIconLabel.frame.size.height/2 - 5, 100, 20)];
        [self.weatherDescLabel setText:@"Snowy"];
        [self.weatherDescLabel setTextColor:[UIColor whiteColor]];
        [self.weatherDescLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]];
        [self.weatherDescLabel setShadowColor:[UIColor blackColor]];
        [self.weatherDescLabel setShadowOffset:CGSizeMake(1, 1)];
        [view addSubview:self.weatherDescLabel];

        self.hiAndLoTemperatureLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, 35, 100, 30)];
        [self.hiAndLoTemperatureLabel setText:@"H   50    L   30"];
        [self.hiAndLoTemperatureLabel setTextColor:[UIColor whiteColor]];
        [self.hiAndLoTemperatureLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:16]];
        [self.hiAndLoTemperatureLabel setShadowColor:[UIColor blackColor]];
        [self.hiAndLoTemperatureLabel setShadowOffset:CGSizeMake(1, 1)];
        [view addSubview:self.hiAndLoTemperatureLabel];

        self.currentTemperatureLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 45, 310, 120)];
        [self.currentTemperatureLabel setText:[NSString stringWithFormat:@"%i℉",55]];
        [self.currentTemperatureLabel setTextColor:[UIColor whiteColor]];
        [self.currentTemperatureLabel setFont:[UIFont fontWithName:@"HelveticaNeue-UltraLight" size:110]];
        [self.currentTemperatureLabel setShadowColor:[UIColor blackColor]];
        [self.currentTemperatureLabel setShadowOffset:CGSizeMake(1, 1)];
        [view addSubview:self.currentTemperatureLabel];

        UIView *box1 = [[UIView alloc] initWithFrame:CGRectMake(5, 160, 310, 125)];
        box1.layer.cornerRadius = 3;
        box1.backgroundColor = [UIColor colorWithWhite:0 alpha:.3];
        [view addSubview:box1];

        //add subviews to box1.
        UILabel *detailsLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 100, 30)];
        [detailsLabel setText:@"Details"];
        [detailsLabel setTextColor:[UIColor whiteColor]];
        [detailsLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:16]];
        [detailsLabel setShadowColor:[UIColor blackColor]];
        [detailsLabel setShadowOffset:CGSizeMake(1, 1)];
        [box1 addSubview:detailsLabel];

        UIView *box1Line = [[UIView alloc] initWithFrame:CGRectMake(10, 30, 290, 0.4)];
        box1Line.layer.cornerRadius = 1;
        box1Line.backgroundColor = [UIColor whiteColor];
        [box1 addSubview:box1Line];

        //add 3 labels as titles.
        UILabel *detailsLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(120, 25, 200, 30)];
        [detailsLabel1 setText:@"Humidity                        70%"];
        [detailsLabel1 setTextColor:[UIColor whiteColor]];
        [detailsLabel1 setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]];
        [detailsLabel1 setShadowColor:[UIColor blackColor]];
        [detailsLabel1 setShadowOffset:CGSizeMake(1, 1)];
        [box1 addSubview:detailsLabel1];

        H3DottedLine *detailsDottedLine1 = [[H3DottedLine alloc] initWithFrame:CGRectMake(41, 19, 180, 50)];
        [detailsDottedLine1 setColor:[UIColor whiteColor]];
        [box1 addSubview:detailsDottedLine1];

        H3DottedLine *detailsDottedLine = [[H3DottedLine alloc] initWithFrame:CGRectMake(41, 28, 180, 50)];
        [detailsDottedLine setColor:[UIColor whiteColor]];
        [box1 addSubview:detailsDottedLine];

        UILabel *detailsLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(120, 55, 200, 30)];
        [detailsLabel2 setText:@"Temprature                     35℉"];
        [detailsLabel2 setTextColor:[UIColor whiteColor]];
        [detailsLabel2 setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]];
        [detailsLabel2 setShadowColor:[UIColor blackColor]];
        [detailsLabel2 setShadowOffset:CGSizeMake(1, 1)];
        [box1 addSubview:detailsLabel2];

        UILabel *detailsLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(120, 85, 200, 30)];
        [detailsLabel3 setText:@"Freezer                            8℉"];
        [detailsLabel3 setTextColor:[UIColor whiteColor]];
        [detailsLabel3 setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]];
        [detailsLabel3 setShadowColor:[UIColor blackColor]];
        [detailsLabel3 setShadowOffset:CGSizeMake(1, 1)];
        [box1 addSubview:detailsLabel3];

        UIImageView *detailsImageView = [[UIImageView alloc] initWithFrame:CGRectMake(30, 50, 50, 50)];
        [detailsImageView setImage:[UIImage imageNamed:@"snowy6 (1)"]];
        [box1 addSubview:detailsImageView];


        //////////////// box2 ////////////////
        UIView *box2 = [[UIView alloc] initWithFrame:CGRectMake(5, 290, 310, 300)];
        box2.layer.cornerRadius = 3;
        box2.backgroundColor = [UIColor colorWithWhite:0 alpha:.3];
        [view addSubview:box2];

        //add subviews to box2.
        UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 100, 30)];
        [tempLabel setText:@"Forcast"];
        [tempLabel setTextColor:[UIColor whiteColor]];
        [tempLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:16]];
        [tempLabel setShadowColor:[UIColor blackColor]];
        [tempLabel setShadowOffset:CGSizeMake(1, 1)];
        [box2 addSubview:tempLabel];

/*

        UILabel *tempStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 100, 200, 100)];
        [tempStatusLabel setText:[NSString stringWithFormat:@"%i℉",18]];
        [tempStatusLabel setTextColor:[UIColor whiteColor]];
        [tempStatusLabel setFont:[UIFont fontWithName:@"HelveticaNeue-UltraLight" size:80]];
        [tempStatusLabel setShadowColor:[UIColor blackColor]];
        [tempStatusLabel setShadowOffset:CGSizeMake(1, 1)];
        [box2 addSubview:tempStatusLabel];

*/

        UIView *box2Line = [[UIView alloc] initWithFrame:CGRectMake(10, 30, 290, 0.4)];
        box2Line.layer.cornerRadius = 1;
        box2Line.backgroundColor = [UIColor whiteColor];
        [box2 addSubview:box2Line];

        //Forcasting TableView
        self.forcastingTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 40, box2.bounds.size.width, box2.bounds.size.height - 40) style:UITableViewStylePlain];
        self.forcastingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.forcastingTableView.backgroundColor = [UIColor clearColor];
        
        [box2 addSubview:self.forcastingTableView];
 


        UIView *box3 = [[UIView alloc] initWithFrame:CGRectMake(5, 595, 310, 125)];
        box3.layer.cornerRadius = 3;
        box3.backgroundColor = [UIColor colorWithWhite:0 alpha:.3];
        //add subviews to box3.
        UILabel *settingsLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 100, 30)];
        [settingsLabel setText:@"Settings"];
        [settingsLabel setTextColor:[UIColor whiteColor]];
        [settingsLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:16]];
        [settingsLabel setShadowColor:[UIColor blackColor]];
        [settingsLabel setShadowOffset:CGSizeMake(1, 1)];
        [box3 addSubview:settingsLabel];

        UIView *box3Line = [[UIView alloc] initWithFrame:CGRectMake(10, 30, 290, 0.4)];
        box3Line.layer.cornerRadius = 1;
        box3Line.backgroundColor = [UIColor whiteColor];
        [box3 addSubview:box3Line];

        UIImageView *alarmImgView = [[UIImageView alloc] initWithFrame:CGRectMake(180, 55, 32, 32)];
        [alarmImgView setImage:[UIImage imageNamed:@"alarm12"]];
        [box3 addSubview:alarmImgView];

        UILabel *alarmlabel = [[UILabel alloc] initWithFrame:CGRectMake(170, 35, 310, 20)];
        [alarmlabel setText:@"DOOR ALARM"];
        [alarmlabel setTextColor:[UIColor whiteColor]];
        [alarmlabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:12]];
        [alarmlabel setShadowColor:[UIColor blackColor]];
        [alarmlabel setShadowOffset:CGSizeMake(1, 1)];
        [box3 addSubview:alarmlabel];

        UILabel *alarmStatuslabel = [[UILabel alloc] initWithFrame:CGRectMake(185, 90, 310, 20)];
        [alarmStatuslabel setText:@"ON"];
        [alarmStatuslabel setTextColor:[UIColor whiteColor]];
        [alarmStatuslabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:12]];
        [alarmStatuslabel setShadowColor:[UIColor blackColor]];
        [alarmStatuslabel setShadowOffset:CGSizeMake(1, 1)];
        [box3 addSubview:alarmStatuslabel];


        H3DottedLine *dottedLine = [[H3DottedLine alloc] initWithFrame:CGRectMake(100, 10, 80, 100)];
        dottedLine.verticleLine=YES;
        [dottedLine setFrame:CGRectMake(20, 30, 290, 50)];
        [dottedLine setColor:[UIColor whiteColor]];
        [box3 addSubview:dottedLine];


        UIImageView *lightImgView = [[UIImageView alloc] initWithFrame:CGRectMake(70, 55, 32, 32)];
        [lightImgView setImage:[UIImage imageNamed:@"glass3"]];
        [box3 addSubview:lightImgView];

        UILabel *lightlabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 35, 310, 20)];
        [lightlabel setText:@"LIGHT"];
        [lightlabel setTextColor:[UIColor whiteColor]];
        [lightlabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:12]];
        [lightlabel setShadowColor:[UIColor blackColor]];
        [lightlabel setShadowOffset:CGSizeMake(1, 1)];
        [box3 addSubview:lightlabel];

        UILabel *lightStatuslabel = [[UILabel alloc] initWithFrame:CGRectMake(75, 90, 310, 20)];
        [lightStatuslabel setText:@"OFF"];
        [lightStatuslabel setTextColor:[UIColor lightGrayColor]];
        [lightStatuslabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:12]];
        //    [lightStatuslabel setShadowColor:[UIColor blackColor]];
        //    [lightStatuslabel setShadowOffset:CGSizeMake(1, 1)];
        [box3 addSubview:lightStatuslabel];
        [view addSubview:box3];
        [self addSubview:view];


    }
    return self;
}

- (UIView *)customViewWith:(UITableView*)tableView
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 705)];

    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, 25, 25)];
    [imageView setImage:[UIImage imageNamed:@"snowy6"]];
    [view addSubview:imageView];

    UILabel *statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(37, 0, 100, 30)];
    [statusLabel setText:@"Snowy"];
    [statusLabel setTextColor:[UIColor whiteColor]];
    [statusLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]];
    [statusLabel setShadowColor:[UIColor blackColor]];
    [statusLabel setShadowOffset:CGSizeMake(1, 1)];
    [view addSubview:statusLabel];

    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 310, 120)];
    [label setText:[NSString stringWithFormat:@"%i℉",40]];
    [label setTextColor:[UIColor whiteColor]];
    [label setFont:[UIFont fontWithName:@"HelveticaNeue-UltraLight" size:110]];
    [label setShadowColor:[UIColor blackColor]];
    [label setShadowOffset:CGSizeMake(1, 1)];
    [view addSubview:label];

    UIView *box1 = [[UIView alloc] initWithFrame:CGRectMake(5, 140, 310, 125)];
    box1.layer.cornerRadius = 3;
    box1.backgroundColor = [UIColor colorWithWhite:0 alpha:.15];
    [view addSubview:box1];

    //add subviews to box1.
    UILabel *detailsLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 100, 30)];
    [detailsLabel setText:@"Details"];
    [detailsLabel setTextColor:[UIColor whiteColor]];
    [detailsLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]];
    [detailsLabel setShadowColor:[UIColor blackColor]];
    [detailsLabel setShadowOffset:CGSizeMake(1, 1)];
    [box1 addSubview:detailsLabel];

    UIView *box1Line = [[UIView alloc] initWithFrame:CGRectMake(10, 30, 290, 0.4)];
    box1Line.layer.cornerRadius = 1;
    box1Line.backgroundColor = [UIColor whiteColor];
    [box1 addSubview:box1Line];

    //add 3 labels as titles.
    UILabel *detailsLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(120, 25, 200, 30)];
    [detailsLabel1 setText:@"Humidity                        70%"];
    [detailsLabel1 setTextColor:[UIColor whiteColor]];
    [detailsLabel1 setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]];
    [detailsLabel1 setShadowColor:[UIColor blackColor]];
    [detailsLabel1 setShadowOffset:CGSizeMake(1, 1)];
    [box1 addSubview:detailsLabel1];

    H3DottedLine *detailsDottedLine1 = [[H3DottedLine alloc] initWithFrame:CGRectMake(41, 19, 180, 50)];
    [detailsDottedLine1 setColor:[UIColor whiteColor]];
    [box1 addSubview:detailsDottedLine1];

    H3DottedLine *detailsDottedLine = [[H3DottedLine alloc] initWithFrame:CGRectMake(41, 28, 180, 50)];
    [detailsDottedLine setColor:[UIColor whiteColor]];
    [box1 addSubview:detailsDottedLine];

    UILabel *detailsLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(120, 55, 200, 30)];
    [detailsLabel2 setText:@"Temprature                     35℉"];
    [detailsLabel2 setTextColor:[UIColor whiteColor]];
    [detailsLabel2 setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]];
    [detailsLabel2 setShadowColor:[UIColor blackColor]];
    [detailsLabel2 setShadowOffset:CGSizeMake(1, 1)];
    [box1 addSubview:detailsLabel2];

    UILabel *detailsLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(120, 85, 200, 30)];
    [detailsLabel3 setText:@"Freezer                            8℉"];
    [detailsLabel3 setTextColor:[UIColor whiteColor]];
    [detailsLabel3 setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]];
    [detailsLabel3 setShadowColor:[UIColor blackColor]];
    [detailsLabel3 setShadowOffset:CGSizeMake(1, 1)];
    [box1 addSubview:detailsLabel3];

    UIImageView *detailsImageView = [[UIImageView alloc] initWithFrame:CGRectMake(30, 50, 50, 50)];
    [detailsImageView setImage:[UIImage imageNamed:@"snowy6 (1)"]];
    [box1 addSubview:detailsImageView];


    //////////////// box2 ////////////////
    UIView *box2 = [[UIView alloc] initWithFrame:CGRectMake(5, 270, 310, 300)];
    box2.layer.cornerRadius = 3;
    box2.backgroundColor = [UIColor colorWithWhite:0 alpha:.15];
    [view addSubview:box2];

    //add subviews to box2.
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 100, 30)];
    [tempLabel setText:@"Temprature"];
    [tempLabel setTextColor:[UIColor whiteColor]];
    [tempLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]];
    [tempLabel setShadowColor:[UIColor blackColor]];
    [tempLabel setShadowOffset:CGSizeMake(1, 1)];
    [box2 addSubview:tempLabel];


    self.forcastingTableView = tableView;

    [box2 addSubview:self.forcastingTableView];

/*
    UILabel *tempStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 100, 200, 100)];
    [tempStatusLabel setText:[NSString stringWithFormat:@"%i℉",18]];
    [tempStatusLabel setTextColor:[UIColor whiteColor]];
    [tempStatusLabel setFont:[UIFont fontWithName:@"HelveticaNeue-UltraLight" size:80]];
    [tempStatusLabel setShadowColor:[UIColor blackColor]];
    [tempStatusLabel setShadowOffset:CGSizeMake(1, 1)];
    [box2 addSubview:tempStatusLabel];



    UIView *box2Line = [[UIView alloc] initWithFrame:CGRectMake(10, 30, 290, 0.4)];
    box2Line.layer.cornerRadius = 1;
    box2Line.backgroundColor = [UIColor whiteColor];
    [box2 addSubview:box2Line];
*/
    UIView *box3 = [[UIView alloc] initWithFrame:CGRectMake(5, 575, 310, 125)];
    box3.layer.cornerRadius = 3;
    box3.backgroundColor = [UIColor colorWithWhite:0 alpha:.15];
    //add subviews to box3.
    UILabel *settingsLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 100, 30)];
    [settingsLabel setText:@"Settings"];
    [settingsLabel setTextColor:[UIColor whiteColor]];
    [settingsLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:14]];
    [settingsLabel setShadowColor:[UIColor blackColor]];
    [settingsLabel setShadowOffset:CGSizeMake(1, 1)];
    [box3 addSubview:settingsLabel];

    UIView *box3Line = [[UIView alloc] initWithFrame:CGRectMake(10, 30, 290, 0.4)];
    box3Line.layer.cornerRadius = 1;
    box3Line.backgroundColor = [UIColor whiteColor];
    [box3 addSubview:box3Line];

    UIImageView *alarmImgView = [[UIImageView alloc] initWithFrame:CGRectMake(180, 55, 32, 32)];
    [alarmImgView setImage:[UIImage imageNamed:@"alarm12"]];
    [box3 addSubview:alarmImgView];

    UILabel *alarmlabel = [[UILabel alloc] initWithFrame:CGRectMake(170, 35, 310, 20)];
    [alarmlabel setText:@"DOOR ALARM"];
    [alarmlabel setTextColor:[UIColor whiteColor]];
    [alarmlabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:12]];
    [alarmlabel setShadowColor:[UIColor blackColor]];
    [alarmlabel setShadowOffset:CGSizeMake(1, 1)];
    [box3 addSubview:alarmlabel];

    UILabel *alarmStatuslabel = [[UILabel alloc] initWithFrame:CGRectMake(185, 90, 310, 20)];
    [alarmStatuslabel setText:@"ON"];
    [alarmStatuslabel setTextColor:[UIColor whiteColor]];
    [alarmStatuslabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:12]];
    [alarmStatuslabel setShadowColor:[UIColor blackColor]];
    [alarmStatuslabel setShadowOffset:CGSizeMake(1, 1)];
    [box3 addSubview:alarmStatuslabel];


    H3DottedLine *dottedLine = [[H3DottedLine alloc] initWithFrame:CGRectMake(100, 10, 80, 100)];
    dottedLine.verticleLine=YES;
    [dottedLine setFrame:CGRectMake(20, 30, 290, 50)];
    [dottedLine setColor:[UIColor whiteColor]];
    [box3 addSubview:dottedLine];


    UIImageView *lightImgView = [[UIImageView alloc] initWithFrame:CGRectMake(70, 55, 32, 32)];
    [lightImgView setImage:[UIImage imageNamed:@"glass3"]];
    [box3 addSubview:lightImgView];

    UILabel *lightlabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 35, 310, 20)];
    [lightlabel setText:@"LIGHT"];
    [lightlabel setTextColor:[UIColor whiteColor]];
    [lightlabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:12]];
    [lightlabel setShadowColor:[UIColor blackColor]];
    [lightlabel setShadowOffset:CGSizeMake(1, 1)];
    [box3 addSubview:lightlabel];

    UILabel *lightStatuslabel = [[UILabel alloc] initWithFrame:CGRectMake(75, 90, 310, 20)];
    [lightStatuslabel setText:@"OFF"];
    [lightStatuslabel setTextColor:[UIColor lightGrayColor]];
    [lightStatuslabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:12]];
    //    [lightStatuslabel setShadowColor:[UIColor blackColor]];
    //    [lightStatuslabel setShadowOffset:CGSizeMake(1, 1)];
    [box3 addSubview:lightStatuslabel];
    [view addSubview:box3];
    
    
    return view;
}


@end
