//
//  SNRGlassyScrollView.h
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/21/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+ImageEffects.h"

//Default blur settings
#define DEFAULT_BLUR_RADIUS 14
#define DEFAULT_BLUR_TINT_COLOR [UIColor colorWithWhite:0 alpha:.3]
#define DEFAULT_BLUR_DELTA_FACTOR 1.4

//How much the background moves when scroll
#define DEFAULT_MAX_BACKGROUND_MOVEMENT_VERTICAL 30
#define DEFAULT_MAX_BACKGROUND_MOVEMENT_HORIZONTAL 150


//Fading space on the top between the view and navigation bar
#define DEFAULT_TOP_FADING_HEIGHT_HALF 10

@protocol SNRGlassyScrollViewDelegate;

@interface SNRGlassyScrollView :  UIView <UIScrollViewDelegate>
//width = 640 + 2 * DEFAULT_MAX_BACKGROUND_MOVEMENT_VERTICAL
//height = 1136 + DEFAULT_MAX_BACKGROUND_MOVEMENT_VERTICAL
@property (nonatomic, strong) UIImage *backgroundImage;
@property (nonatomic, strong) UIImage *blurredBackgroundImage;//default blurred is provided, thus nil is acceptable
@property (nonatomic, assign) CGFloat viewDistanceFromBottom;//how much view is showed up from the bottom
@property (nonatomic, strong) UIView *foregroundView;//the view that will contain all the info
@property (nonatomic, assign) CGFloat topLayoutGuideLength;//set this only when using navigation bar of sorts.
@property (nonatomic, strong) UIScrollView *foregroundScrollView;

//  YES if view contains weather data
@property (assign, nonatomic) BOOL hasData;

//  YES if view contains local weather data
@property (assign, nonatomic, getter = isLocal) BOOL local;

@property (nonatomic, weak) id<SNRGlassyScrollViewDelegate> delegate;

- (id)initWithFrame:(CGRect)frame BackgroundImage:(UIImage *)backgroundImage blurredImage:(UIImage *)blurredImage viewDistanceFromBottom:(CGFloat)viewDistanceFromBottom foregroundView:(UIView *)foregroundView;
- (void)scrollHorizontalRatio:(CGFloat)ratio;//from -1 to 1
- (void)scrollVerticallyToOffset:(CGFloat)offsetY;
// change background image on the go
- (void)setBackgroundImage:(UIImage *)backgroundImage overWriteBlur:(BOOL)overWriteBlur animated:(BOOL)animated duration:(NSTimeInterval)interval;
- (void)blurBackground:(BOOL)shouldBlur;
@end


@protocol SNRGlassyScrollViewDelegate <NSObject>
@optional
//use this to configure your foregroundView when the frame of the whole view changed
- (void)glassScrollView:(SNRGlassyScrollView *)glassScrollView didChangedToFrame:(CGRect)frame;
//make custom blur without messing with default settings
- (UIImage*)glassScrollView:(SNRGlassyScrollView *)glassScrollView blurForImage:(UIImage *)image;


@end
