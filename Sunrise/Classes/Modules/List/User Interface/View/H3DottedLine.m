//
//  H3DottedLine.m
//  BTGlassScrollViewExample
//
//  Created by smapps on 4/15/15.
//  Copyright (c) 2015 Byte. All rights reserved.
//

#import "H3DottedLine.h"

@implementation H3DottedLine

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
#pragma mark - Object Lifecycle
-(instancetype)initWithFrame:(CGRect)frame{

    NSLog(@"init");
    self = [super initWithFrame:frame];
    
    if (self) {
        // Set Default Values
        _thickness = 1.0f;
        _color = [UIColor grayColor];
        _dashedGap = 2.0f;
        _dashedLength = 1.0f;
        [self updateLineStartingAt:self.frame.origin andEndPoint:CGPointMake(self.frame.origin.x+self.frame.size.width, self.frame.origin.y)];
        
    }
    
    return self;
}
/*
- (instancetype)init {
    NSLog(@"init");
self = [super init];

    if (self) {
        // Set Default Values
        _thickness = 1.0f;
        _color = [UIColor grayColor];
        _dashedGap = 2.0f;
        _dashedLength = 1.0f;
        [self updateLineStartingAt:self.frame.origin andEndPoint:CGPointMake(self.frame.origin.x+self.frame.size.width, self.frame.origin.y)];

    }
    
    return self;
}
 */

#pragma mark - View Lifecycle

- (void)layoutSubviews {
//    NSLog(@"layoutSubviews");
    // Note, this object draws a straight line. If you wanted the line at an angle you simply need to adjust the start and/or end point here.
}

#pragma mark - Setters

- (void)setThickness:(CGFloat)thickness {
    _thickness = thickness;
    [self setNeedsLayout];
}

- (void)setColor:(UIColor *)color {
    _color = [color copy];
    [self setNeedsLayout];
}

- (void)setDashedGap:(CGFloat)dashedGap {
    _dashedGap = dashedGap;
    [self setNeedsLayout];
}

- (void)setDashedLength:(CGFloat)dashedLength {
    _dashedLength = dashedLength;
    [self setNeedsLayout];
}

#pragma mark - Draw Methods

-(void)updateLineStartingAt:(CGPoint)beginPoint andEndPoint:(CGPoint)endPoint {
//    NSLog(@"updateLineStartingAt");
    // Important, otherwise we will be adding multiple sub layers
    if ([[[self layer] sublayers] objectAtIndex:0]) {
        self.layer.sublayers = nil;
    }
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    [shapeLayer setBounds:self.bounds];
    [shapeLayer setPosition:self.center];
    [shapeLayer setFillColor:[UIColor clearColor].CGColor];
    [shapeLayer setStrokeColor:self.color.CGColor];
    [shapeLayer setLineJoin:kCALineJoinRound];
    [shapeLayer setLineCap:kCALineCapRound];
    [shapeLayer setLineDashPattern:@[@(self.dashedLength), @(self.dashedGap)]];
    
    // Setup the path
    CGMutablePathRef path = CGPathCreateMutable();
    if (beginPoint.x==100) {
        NSLog(@"Here...1.1.1.");
            CGPathMoveToPoint(path, NULL, 20, 4);
            CGPathAddLineToPoint(path, NULL, 20, 70);
    }
    else{
        
        CGPathMoveToPoint(path, NULL, beginPoint.x, beginPoint.y);
        CGPathAddLineToPoint(path, NULL, endPoint.x, endPoint.y);
    }

//    CGPathMoveToPoint(path, NULL, 120, 10);
//    CGPathAddLineToPoint(path, NULL, 120, 80);
    
    [shapeLayer setPath:path];
    CGPathRelease(path);
    
    [[self layer] addSublayer:shapeLayer];
}

@end
