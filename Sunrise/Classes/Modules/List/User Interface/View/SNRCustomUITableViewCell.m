//
//  SNRCustomUITableViewCell.m
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/23/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import "SNRCustomUITableViewCell.h"

@implementation SNRCustomUITableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];

        self.forcastweekDay =[[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 30)];
        self.forcastweekDay.textColor = [UIColor whiteColor];
        self.forcastweekDay.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.];
        [self addSubview:self.forcastweekDay];

        self.minTemp =[[UILabel alloc] initWithFrame:CGRectMake(self.bounds.size.width - 50, 10, 30, 30)];
        self.minTemp.textColor = [UIColor colorWithRed:52.0/255 green:152.0/255 blue:219.0/255 alpha:1.0];
        self.minTemp.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.];
        [self addSubview:self.minTemp];

        self.maxTemp =[[UILabel alloc] initWithFrame:CGRectMake(self.bounds.size.width - 100, 10, 30, 30)];
//        self.maxTemp.textColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"gradient5"]];

        self.maxTemp.textColor = [UIColor whiteColor];
        self.maxTemp.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.];
        [self addSubview:self.maxTemp];

    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.imageView.frame = CGRectOffset(self.imageView.frame, 6, 0);
    self.textLabel.frame = CGRectOffset(self.textLabel.frame, 6, 0);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
