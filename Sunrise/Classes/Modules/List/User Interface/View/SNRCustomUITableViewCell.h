//
//  SNRCustomUITableViewCell.h
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/23/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNRCustomUITableViewCell : UITableViewCell
@property(nonatomic,retain) UILabel *forcastweekDay;
@property(nonatomic,retain) UILabel *maxTemp;
@property(nonatomic,retain) UILabel *minTemp;

@end
