//
//  H3DottedLine.h
//  BTGlassScrollViewExample
//
//  Created by smapps on 4/15/15.
//  Copyright (c) 2015 Byte. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Simple UIView for a dotted line
 */
@interface H3DottedLine : UIView

/**
 *  Set the line's thickness
 */
@property (nonatomic, assign) CGFloat thickness;

/**
 *  Set the line's color
 */
@property (nonatomic, copy) UIColor *color;

/**
 *  Set the length of the dash
 */
@property (nonatomic, assign) CGFloat dashedLength;

/**
 *  Set the gap between dashes
 */
@property (nonatomic, assign) CGFloat dashedGap;


@property (nonatomic) BOOL verticleLine;

@end