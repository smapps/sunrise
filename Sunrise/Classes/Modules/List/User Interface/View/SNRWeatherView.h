//
//  SNRWeatherView.h
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/22/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 SOLWeatherView is used to display weather data for a single location to the user. Every instance
 of SOLWeatherView is managed by SOLMainViewController only.
 */

@interface SNRWeatherView : UIView

// -----
// @name Properties
//

@property(nonatomic, retain) UILabel *weatherDescLabel;

@property(nonatomic, retain) UILabel *currentTemperatureLabel;

@property(nonatomic, retain) UILabel *hiAndLoTemperatureLabel;

//  Displays the icon for current conditions
@property (nonatomic, retain) UILabel *conditionIconLabel;

//  TableView that displayes weather forcasting data.
@property (nonatomic, retain) UITableView *forcastingTableView;

- (UIView *)customViewWith:(UITableView*)tableView;

@end
