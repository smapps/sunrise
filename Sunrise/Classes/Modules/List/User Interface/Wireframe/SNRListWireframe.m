//
//  SNRListWireframe.m
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/21/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import "SNRListWireframe.h"

static NSString *ListViewControllerIdentifier = @"SNRMainViewController";

@implementation SNRListWireframe

- (void)presentListInterfaceFromWindow:(UIWindow *)window
{
//    SNRMainViewController *listViewController = [[SNRMainViewController alloc] init];
    SNRMainViewController *listViewController = [self listViewControllerFromStoryboard];

    [self.rootWireframe showRootViewController:listViewController
                                      inWindow:window];
}

- (SNRMainViewController *)listViewControllerFromStoryboard
{
    UIStoryboard *storyboard = [self mainStoryboard];
    SNRMainViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:ListViewControllerIdentifier];

    return viewController;
}


- (UIStoryboard *)mainStoryboard
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle:[NSBundle mainBundle]];

    return storyboard;
}

@end
