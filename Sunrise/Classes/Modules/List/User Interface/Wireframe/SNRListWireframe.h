//
//  SNRListWireframe.h
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/21/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SNRMainViewController.h"
#import "SNRRootWireframe.h"

@interface SNRListWireframe : NSObject

@property (nonatomic, strong) SNRRootWireframe *rootWireframe;

- (void)presentListInterfaceFromWindow:(UIWindow *)window;
//- (void)presentAddInterface;

@end
