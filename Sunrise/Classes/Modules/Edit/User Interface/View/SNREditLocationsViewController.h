//
//  SNREditLocationsViewController.h
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/22/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNREditLocationsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

// -----
// @name Properties
// -----

// List of location metadata to display in the locations table view
@property (strong, nonatomic)           NSMutableArray      *locations;

@end
