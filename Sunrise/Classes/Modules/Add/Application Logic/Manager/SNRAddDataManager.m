//
//  SNRAddDataManager.m
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/21/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import "SNRAddDataManager.h"
#import "City.h"
#import "SNRCoreDataStore.h"
#import "SNRCityItem.h"

@implementation SNRAddDataManager

- (void)addNewEntry:(SNRCityItem *)entry
{
    City *newEntry = [self.dataStore newCity];
    newEntry.areaName = entry.areaName;
    newEntry.name = entry.name;

    [self.dataStore save];
}

@end
