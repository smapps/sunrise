//
//  SNRAddDataManager.h
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/21/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SNRCoreDataStore;
@class SNRCityItem;

@interface SNRAddDataManager : NSObject

@property (nonatomic, strong) SNRCoreDataStore *dataStore;

- (void)addNewEntry:(SNRCityItem *)entry;

@end
