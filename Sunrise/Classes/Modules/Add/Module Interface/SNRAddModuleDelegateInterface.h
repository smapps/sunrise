//
//  SNRAddModuleDelegateInterface.h
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/21/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import <Foundation/Foundation.h>

// This protocol becomes the way by which something else can hear back about what happened inside of this module
@protocol SNRAddModuleDelegateInterface <NSObject>

- (void)addModuleDidCancelAddAction;
- (void)addModuleDidSaveAddAction;

@end
