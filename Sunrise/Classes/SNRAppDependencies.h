//
//  SNRAppDependencies.h
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/21/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SNRAppDependencies : NSObject

- (void)installRootViewControllerIntoWindow:(UIWindow *)window;

@end
