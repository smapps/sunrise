//
//  SNRCoreDataStore.h
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/21/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CoreData/CoreData.h>

@class City;

typedef void(^SNRDataStoreFetchCompletionBlock)(NSArray *results);

@interface SNRCoreDataStore : NSObject


- (void)fetchEntriesWithPredicate:(NSPredicate *)predicate
                  sortDescriptors:(NSArray *)sortDescriptors
                  completionBlock:(SNRDataStoreFetchCompletionBlock)completionBlock;

- (City *)newCity;

- (void)save;

@end
