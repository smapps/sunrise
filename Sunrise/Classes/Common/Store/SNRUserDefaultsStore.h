//
//  SNRUserDefaultsStore.h
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/24/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SNRWeatherData;

typedef enum {
    SNRFahrenheitScale = 0,
    SNRCelsiusScale
} SNRTemperatureScale;

/**
 SNRUserDefaultsStore allows for easy state persistence and acts as wrapper around NSUserDefaults
 */
@interface SNRUserDefaultsStore : NSObject

// -----
// @name Using the State Manager
// -----

/**
 Get the saved temperature scale
 @returns The saved temperature scale
 */
+ (SNRTemperatureScale)temperatureScale;

/**
 Save the given temperature scale
 @param scale Temperature scale to save
 */
+ (void)setTemperatureScale:(SNRTemperatureScale)scale;

/**
 Get saved weather data
 @returns Saved weather data as a dictionary
 */
+ (NSDictionary *)weatherData;

/**
 Save the given weather data
 @param weatherData Weather data to save
 */
+ (void)setWeatherData:(NSDictionary *)weatherData;

/**
 Get the saved ordered-list of weather tags
 @returns The saved weather tags
 */
+ (NSArray *)weatherTags;

/**
 Save the given ordered-list of weather tags
 @param weatherTags List of weather tags
 */
+ (void)setWeatherTags:(NSArray *)weatherTags;

@end
