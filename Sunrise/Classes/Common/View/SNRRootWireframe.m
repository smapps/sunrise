//
//  SNRRootWireframe.m
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/21/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import "SNRRootWireframe.h"

@implementation SNRRootWireframe

- (void)showRootViewController:(UIViewController *)viewController
                      inWindow:(UIWindow *)window
{
    UINavigationController *navigationController = [self navigationControllerFromWindow:window];
    navigationController.viewControllers = @[viewController];
}


- (UINavigationController *)navigationControllerFromWindow:(UIWindow *)window
{
    UINavigationController *navigationController = (UINavigationController *)[window rootViewController];

    return navigationController;
}

@end
