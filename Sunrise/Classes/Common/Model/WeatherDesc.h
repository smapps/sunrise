//
//	WeatherDesc.h
//
//	Create by sameh mabrouk on 21/1/2016
//	Copyright © 2016. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeatherDesc : NSObject

@property NSString * value;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end