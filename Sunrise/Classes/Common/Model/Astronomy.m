//
//  Astronomy.m
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/21/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import "Astronomy.h"

@implementation Astronomy

-(instancetype)initWithDictionary:(NSDictionary *)dictionary{

    self = [super init];

    self.moonrise = dictionary[@"moonrise"];

    self.moonset = dictionary[@"moonset"];

    self.sunrise = dictionary[@"sunrise"];

    self.sunset = dictionary[@"sunset"];

    return self;
}

-(NSDictionary *)toDictionary
{
    NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
       if(self.moonrise != nil){
        dictionary[@"moonrise"] = self.moonrise;
    }
    if(self.moonset != nil){
        dictionary[@"moonset"] = self.moonset;
    }
    if(self.sunrise != nil){
        dictionary[@"sunrise"] = self.sunrise;
    }
    if(self.sunset != nil){
        dictionary[@"sunset"] = self.sunset;
    }
    
    return dictionary;
    
}

@end
