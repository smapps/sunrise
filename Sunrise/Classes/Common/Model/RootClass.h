//
//	RootClass.h
//
//	Create by sameh mabrouk on 21/1/2016
//	Copyright © 2016. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Data.h"

@interface RootClass : NSObject

@property Data * data;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end