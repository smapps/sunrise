//
//  WeatherItem.h
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/21/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeatherItem : NSObject

@property (nonatomic, strong)   NSDate*     dueDate;
@property (nonatomic, copy)     NSString*   name;

+ (instancetype)todoItemWithDueDate:(NSDate *)dueDate name:(NSString *)name;

@end
