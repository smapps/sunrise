//
//	Weather.h
//
//	Create by sameh mabrouk on 21/1/2016
//	Copyright © 2016. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Astronomy.h"
#import "Hourly.h"

@interface Weather : NSObject

@property NSArray * astronomy;
@property NSString * date;
@property NSArray * hourly;
@property NSString * maxtempC;
@property NSString * maxtempF;
@property NSString * mintempC;
@property NSString * mintempF;
@property NSString * uvIndex;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end