//
//  Astronomy.h
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/21/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Astronomy : NSObject

@property NSString * moonrise;
@property NSString * moonset;
@property NSString * sunrise;
@property NSString * sunset;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;

@end
