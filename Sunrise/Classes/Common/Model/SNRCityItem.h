//
//  SNRCityItem.h
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/21/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SNRCityItem : NSObject

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *areaName;
@property (nullable, nonatomic, retain) NSNumber *longitude;
@property (nullable, nonatomic, retain) NSNumber *latitude;

+ (instancetype)cityWithName:(NSString *)name area:(NSString *)area;

@end
