//
//	Data.h
//
//	Create by sameh mabrouk on 21/1/2016
//	Copyright © 2016. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CurrentCondition.h"
//#import "Request.h"
#import "Weather.h"

@interface Data : NSObject

@property NSArray * currentCondition;
@property NSArray * request;
@property NSArray * weather;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end