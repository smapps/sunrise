//
//	Hourly.m
//
//	Create by sameh mabrouk on 21/1/2016
//	Copyright © 2016. All rights reserved.
//



#import "Hourly.h"

@interface Hourly ()
@end
@implementation Hourly




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	self.DewPointC = dictionary[@"DewPointC"];

	self.DewPointF = dictionary[@"DewPointF"];

	self.FeelsLikeC = dictionary[@"FeelsLikeC"];

	self.FeelsLikeF = dictionary[@"FeelsLikeF"];

	self.HeatIndexC = dictionary[@"HeatIndexC"];

	self.HeatIndexF = dictionary[@"HeatIndexF"];

	self.WindChillC = dictionary[@"WindChillC"];

	self.WindChillF = dictionary[@"WindChillF"];

	self.WindGustKmph = dictionary[@"WindGustKmph"];

	self.WindGustMiles = dictionary[@"WindGustMiles"];

	self.chanceoffog = dictionary[@"chanceoffog"];

	self.chanceoffrost = dictionary[@"chanceoffrost"];

	self.chanceofhightemp = dictionary[@"chanceofhightemp"];

	self.chanceofovercast = dictionary[@"chanceofovercast"];

	self.chanceofrain = dictionary[@"chanceofrain"];

	self.chanceofremdry = dictionary[@"chanceofremdry"];

	self.chanceofsnow = dictionary[@"chanceofsnow"];

	self.chanceofsunshine = dictionary[@"chanceofsunshine"];

	self.chanceofthunder = dictionary[@"chanceofthunder"];

	self.chanceofwindy = dictionary[@"chanceofwindy"];

	self.cloudcover = dictionary[@"cloudcover"];

	self.humidity = dictionary[@"humidity"];

	self.precipMM = dictionary[@"precipMM"];

	self.pressure = dictionary[@"pressure"];

	self.tempC = dictionary[@"tempC"];

	self.tempF = dictionary[@"tempF"];

	self.time = dictionary[@"time"];

	self.visibility = dictionary[@"visibility"];

	self.weatherCode = dictionary[@"weatherCode"];
/*
	if(dictionary[@"weatherDesc"] != nil){
		NSArray * weatherDescDictionaries = dictionary[@"weatherDesc"];
		NSMutableArray * weatherDescItems = [NSMutableArray array];
		for(NSDictionary * weatherDescDictionary in weatherDescDictionaries){
			WeatherDesc * weatherDescItem = [[WeatherDesc alloc] initWithDictionary:weatherDescDictionary];
			[weatherDescItems addObject:weatherDescItem];
		}
		self.weatherDesc = weatherDescItems;
	}
	if(dictionary[@"weatherIconUrl"] != nil){
		NSArray * weatherIconUrlDictionaries = dictionary[@"weatherIconUrl"];
		NSMutableArray * weatherIconUrlItems = [NSMutableArray array];
		for(NSDictionary * weatherIconUrlDictionary in weatherIconUrlDictionaries){
			WeatherIconUrl * weatherIconUrlItem = [[WeatherIconUrl alloc] initWithDictionary:weatherIconUrlDictionary];
			[weatherIconUrlItems addObject:weatherIconUrlItem];
		}
		self.weatherIconUrl = weatherIconUrlItems;
 */
//	}

	self.winddir16Point = dictionary[@"winddir16Point"];

	self.winddirDegree = dictionary[@"winddirDegree"];

	self.windspeedKmph = dictionary[@"windspeedKmph"];

	self.windspeedMiles = dictionary[@"windspeedMiles"];

	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.DewPointC != nil){
		dictionary[@"DewPointC"] = self.DewPointC;
	}
	if(self.DewPointF != nil){
		dictionary[@"DewPointF"] = self.DewPointF;
	}
	if(self.FeelsLikeC != nil){
		dictionary[@"FeelsLikeC"] = self.FeelsLikeC;
	}
	if(self.FeelsLikeF != nil){
		dictionary[@"FeelsLikeF"] = self.FeelsLikeF;
	}
	if(self.HeatIndexC != nil){
		dictionary[@"HeatIndexC"] = self.HeatIndexC;
	}
	if(self.HeatIndexF != nil){
		dictionary[@"HeatIndexF"] = self.HeatIndexF;
	}
	if(self.WindChillC != nil){
		dictionary[@"WindChillC"] = self.WindChillC;
	}
	if(self.WindChillF != nil){
		dictionary[@"WindChillF"] = self.WindChillF;
	}
	if(self.WindGustKmph != nil){
		dictionary[@"WindGustKmph"] = self.WindGustKmph;
	}
	if(self.WindGustMiles != nil){
		dictionary[@"WindGustMiles"] = self.WindGustMiles;
	}
	if(self.chanceoffog != nil){
		dictionary[@"chanceoffog"] = self.chanceoffog;
	}
	if(self.chanceoffrost != nil){
		dictionary[@"chanceoffrost"] = self.chanceoffrost;
	}
	if(self.chanceofhightemp != nil){
		dictionary[@"chanceofhightemp"] = self.chanceofhightemp;
	}
	if(self.chanceofovercast != nil){
		dictionary[@"chanceofovercast"] = self.chanceofovercast;
	}
	if(self.chanceofrain != nil){
		dictionary[@"chanceofrain"] = self.chanceofrain;
	}
	if(self.chanceofremdry != nil){
		dictionary[@"chanceofremdry"] = self.chanceofremdry;
	}
	if(self.chanceofsnow != nil){
		dictionary[@"chanceofsnow"] = self.chanceofsnow;
	}
	if(self.chanceofsunshine != nil){
		dictionary[@"chanceofsunshine"] = self.chanceofsunshine;
	}
	if(self.chanceofthunder != nil){
		dictionary[@"chanceofthunder"] = self.chanceofthunder;
	}
	if(self.chanceofwindy != nil){
		dictionary[@"chanceofwindy"] = self.chanceofwindy;
	}
	if(self.cloudcover != nil){
		dictionary[@"cloudcover"] = self.cloudcover;
	}
	if(self.humidity != nil){
		dictionary[@"humidity"] = self.humidity;
	}
	if(self.precipMM != nil){
		dictionary[@"precipMM"] = self.precipMM;
	}
	if(self.pressure != nil){
		dictionary[@"pressure"] = self.pressure;
	}
	if(self.tempC != nil){
		dictionary[@"tempC"] = self.tempC;
	}
	if(self.tempF != nil){
		dictionary[@"tempF"] = self.tempF;
	}
	if(self.time != nil){
		dictionary[@"time"] = self.time;
	}
	if(self.visibility != nil){
		dictionary[@"visibility"] = self.visibility;
	}
	if(self.weatherCode != nil){
		dictionary[@"weatherCode"] = self.weatherCode;
	}
	if(self.weatherDesc != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
        /*
		for(WeatherDesc * weatherDescElement in self.weatherDesc){
			[dictionaryElements addObject:[weatherDescElement toDictionary]];
		}
         */
		dictionary[@"weatherDesc"] = dictionaryElements;
	}
	if(self.weatherIconUrl != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
        /*
		for(WeatherIconUrl * weatherIconUrlElement in self.weatherIconUrl){
			[dictionaryElements addObject:[weatherIconUrlElement toDictionary]];
		}
         */
		dictionary[@"weatherIconUrl"] = dictionaryElements;
	}
	if(self.winddir16Point != nil){
		dictionary[@"winddir16Point"] = self.winddir16Point;
	}
	if(self.winddirDegree != nil){
		dictionary[@"winddirDegree"] = self.winddirDegree;
	}
	if(self.windspeedKmph != nil){
		dictionary[@"windspeedKmph"] = self.windspeedKmph;
	}
	if(self.windspeedMiles != nil){
		dictionary[@"windspeedMiles"] = self.windspeedMiles;
	}
	return dictionary;

}
@end