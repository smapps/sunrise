//
//	Hourly.h
//
//	Create by sameh mabrouk on 21/1/2016
//	Copyright © 2016. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface Hourly : NSObject

@property NSString * DewPointC;
@property NSString * DewPointF;
@property NSString * FeelsLikeC;
@property NSString * FeelsLikeF;
@property NSString * HeatIndexC;
@property NSString * HeatIndexF;
@property NSString * WindChillC;
@property NSString * WindChillF;
@property NSString * WindGustKmph;
@property NSString * WindGustMiles;
@property NSString * chanceoffog;
@property NSString * chanceoffrost;
@property NSString * chanceofhightemp;
@property NSString * chanceofovercast;
@property NSString * chanceofrain;
@property NSString * chanceofremdry;
@property NSString * chanceofsnow;
@property NSString * chanceofsunshine;
@property NSString * chanceofthunder;
@property NSString * chanceofwindy;
@property NSString * cloudcover;
@property NSString * humidity;
@property NSString * precipMM;
@property NSString * pressure;
@property NSString * tempC;
@property NSString * tempF;
@property NSString * time;
@property NSString * visibility;
@property NSString * weatherCode;
@property NSArray * weatherDesc;
@property NSArray * weatherIconUrl;
@property NSString * winddir16Point;
@property NSString * winddirDegree;
@property NSString * windspeedKmph;
@property NSString * windspeedMiles;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end