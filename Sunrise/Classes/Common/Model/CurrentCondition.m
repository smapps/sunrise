//
//	CurrentCondition.m
//
//	Create by sameh mabrouk on 21/1/2016
//	Copyright © 2016. All rights reserved.
//



#import "CurrentCondition.h"

@interface CurrentCondition ()
@end
@implementation CurrentCondition




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	self.FeelsLikeC = dictionary[@"FeelsLikeC"];

	self.FeelsLikeF = dictionary[@"FeelsLikeF"];

	self.cloudcover = dictionary[@"cloudcover"];

	self.humidity = dictionary[@"humidity"];

	self.observationTime = dictionary[@"observation_time"];

	self.precipMM = dictionary[@"precipMM"];

	self.pressure = dictionary[@"pressure"];

	self.tempC = dictionary[@"temp_C"];

	self.tempF = dictionary[@"temp_F"];

	self.visibility = dictionary[@"visibility"];

	self.weatherCode = dictionary[@"weatherCode"];

	if(dictionary[@"weatherDesc"] != nil){
		NSArray * weatherDescDictionaries = dictionary[@"weatherDesc"];
		NSMutableArray * weatherDescItems = [NSMutableArray array];
		for(NSDictionary * weatherDescDictionary in weatherDescDictionaries){
			WeatherDesc * weatherDescItem = [[WeatherDesc alloc] initWithDictionary:weatherDescDictionary];
			[weatherDescItems addObject:weatherDescItem];
		}
		self.weatherDesc = weatherDescItems;
	}
	if(dictionary[@"weatherIconUrl"] != nil){
		NSArray * weatherIconUrlDictionaries = dictionary[@"weatherIconUrl"];
		NSMutableArray * weatherIconUrlItems = [NSMutableArray array];
		for(NSDictionary * weatherIconUrlDictionary in weatherIconUrlDictionaries){
			WeatherIconUrl * weatherIconUrlItem = [[WeatherIconUrl alloc] initWithDictionary:weatherIconUrlDictionary];
			[weatherIconUrlItems addObject:weatherIconUrlItem];
		}
		self.weatherIconUrl = weatherIconUrlItems;
	}
	self.winddir16Point = dictionary[@"winddir16Point"];

	self.winddirDegree = dictionary[@"winddirDegree"];

	self.windspeedKmph = dictionary[@"windspeedKmph"];

	self.windspeedMiles = dictionary[@"windspeedMiles"];

	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.FeelsLikeC != nil){
		dictionary[@"FeelsLikeC"] = self.FeelsLikeC;
	}
	if(self.FeelsLikeF != nil){
		dictionary[@"FeelsLikeF"] = self.FeelsLikeF;
	}
	if(self.cloudcover != nil){
		dictionary[@"cloudcover"] = self.cloudcover;
	}
	if(self.humidity != nil){
		dictionary[@"humidity"] = self.humidity;
	}
	if(self.observationTime != nil){
		dictionary[@"observation_time"] = self.observationTime;
	}
	if(self.precipMM != nil){
		dictionary[@"precipMM"] = self.precipMM;
	}
	if(self.pressure != nil){
		dictionary[@"pressure"] = self.pressure;
	}
	if(self.tempC != nil){
		dictionary[@"temp_C"] = self.tempC;
	}
	if(self.tempF != nil){
		dictionary[@"temp_F"] = self.tempF;
	}
	if(self.visibility != nil){
		dictionary[@"visibility"] = self.visibility;
	}
	if(self.weatherCode != nil){
		dictionary[@"weatherCode"] = self.weatherCode;
	}
	if(self.weatherDesc != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
		for(WeatherDesc * weatherDescElement in self.weatherDesc){
			[dictionaryElements addObject:[weatherDescElement toDictionary]];
		}
		dictionary[@"weatherDesc"] = dictionaryElements;
	}
	if(self.weatherIconUrl != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
		for(WeatherIconUrl * weatherIconUrlElement in self.weatherIconUrl){
			[dictionaryElements addObject:[weatherIconUrlElement toDictionary]];
		}
		dictionary[@"weatherIconUrl"] = dictionaryElements;
	}
	if(self.winddir16Point != nil){
		dictionary[@"winddir16Point"] = self.winddir16Point;
	}
	if(self.winddirDegree != nil){
		dictionary[@"winddirDegree"] = self.winddirDegree;
	}
	if(self.windspeedKmph != nil){
		dictionary[@"windspeedKmph"] = self.windspeedKmph;
	}
	if(self.windspeedMiles != nil){
		dictionary[@"windspeedMiles"] = self.windspeedMiles;
	}
	return dictionary;

}
@end