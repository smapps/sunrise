//
//	Weather.m
//
//	Create by sameh mabrouk on 21/1/2016
//	Copyright © 2016. All rights reserved.
//



#import "Weather.h"

@interface Weather ()
@end
@implementation Weather




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(dictionary[@"astronomy"] != nil){
		NSArray * astronomyDictionaries = dictionary[@"astronomy"];
		NSMutableArray * astronomyItems = [NSMutableArray array];
		for(NSDictionary * astronomyDictionary in astronomyDictionaries){
			Astronomy * astronomyItem = [[Astronomy alloc] initWithDictionary:astronomyDictionary];
			[astronomyItems addObject:astronomyItem];
		}
		self.astronomy = astronomyItems;
	}
	self.date = dictionary[@"date"];

	if(dictionary[@"hourly"] != nil){
		NSArray * hourlyDictionaries = dictionary[@"hourly"];
		NSMutableArray * hourlyItems = [NSMutableArray array];
		for(NSDictionary * hourlyDictionary in hourlyDictionaries){
			Hourly * hourlyItem = [[Hourly alloc] initWithDictionary:hourlyDictionary];
			[hourlyItems addObject:hourlyItem];
		}
		self.hourly = hourlyItems;
	}
	self.maxtempC = dictionary[@"maxtempC"];

	self.maxtempF = dictionary[@"maxtempF"];

	self.mintempC = dictionary[@"mintempC"];

	self.mintempF = dictionary[@"mintempF"];

	self.uvIndex = dictionary[@"uvIndex"];

	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.astronomy != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
		for(Astronomy * astronomyElement in self.astronomy){
			[dictionaryElements addObject:[astronomyElement toDictionary]];
		}
		dictionary[@"astronomy"] = dictionaryElements;
	}
	if(self.date != nil){
		dictionary[@"date"] = self.date;
	}
	if(self.hourly != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
		for(Hourly * hourlyElement in self.hourly){
			[dictionaryElements addObject:[hourlyElement toDictionary]];
		}
		dictionary[@"hourly"] = dictionaryElements;
	}
	if(self.maxtempC != nil){
		dictionary[@"maxtempC"] = self.maxtempC;
	}
	if(self.maxtempF != nil){
		dictionary[@"maxtempF"] = self.maxtempF;
	}
	if(self.mintempC != nil){
		dictionary[@"mintempC"] = self.mintempC;
	}
	if(self.mintempF != nil){
		dictionary[@"mintempF"] = self.mintempF;
	}
	if(self.uvIndex != nil){
		dictionary[@"uvIndex"] = self.uvIndex;
	}
	return dictionary;

}
@end