//
//	CurrentCondition.h
//
//	Create by sameh mabrouk on 21/1/2016
//	Copyright © 2016. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeatherDesc.h"
#import "WeatherIconUrl.h"

@interface CurrentCondition : NSObject

@property NSString * FeelsLikeC;
@property NSString * FeelsLikeF;
@property NSString * cloudcover;
@property NSString * humidity;
@property NSString * observationTime;
@property NSString * precipMM;
@property NSString * pressure;
@property NSString * tempC;
@property NSString * tempF;
@property NSString * visibility;
@property NSString * weatherCode;
@property NSArray * weatherDesc;
@property NSArray * weatherIconUrl;
@property NSString * winddir16Point;
@property NSString * winddirDegree;
@property NSString * windspeedKmph;
@property NSString * windspeedMiles;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end