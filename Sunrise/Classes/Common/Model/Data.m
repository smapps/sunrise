//
//	Data.m
//
//	Create by sameh mabrouk on 21/1/2016
//	Copyright © 2016. All rights reserved.
//



#import "Data.h"

@interface Data ()
@end
@implementation Data




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(dictionary[@"current_condition"] != nil){
		NSArray * currentConditionDictionaries = dictionary[@"current_condition"];
		NSMutableArray * currentConditionItems = [NSMutableArray array];
		for(NSDictionary * currentConditionDictionary in currentConditionDictionaries){
			CurrentCondition * currentConditionItem = [[CurrentCondition alloc] initWithDictionary:currentConditionDictionary];
			[currentConditionItems addObject:currentConditionItem];
		}
		self.currentCondition = currentConditionItems;
	}
    /*
	if(dictionary[@"request"] != nil){
		NSArray * requestDictionaries = dictionary[@"request"];
		NSMutableArray * requestItems = [NSMutableArray array];
		for(NSDictionary * requestDictionary in requestDictionaries){
			Request * requestItem = [[Request alloc] initWithDictionary:requestDictionary];
			[requestItems addObject:requestItem];
		}
		self.request = requestItems;
	}
     */
	if(dictionary[@"weather"] != nil){
		NSArray * weatherDictionaries = dictionary[@"weather"];
		NSMutableArray * weatherItems = [NSMutableArray array];
		for(NSDictionary * weatherDictionary in weatherDictionaries){
			Weather * weatherItem = [[Weather alloc] initWithDictionary:weatherDictionary];
			[weatherItems addObject:weatherItem];
		}
		self.weather = weatherItems;
	}
	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.currentCondition != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
		for(CurrentCondition * currentConditionElement in self.currentCondition){
			[dictionaryElements addObject:[currentConditionElement toDictionary]];
		}
		dictionary[@"current_condition"] = dictionaryElements;
	}
    /*
	if(self.request != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
		for(Request * requestElement in self.request){
			[dictionaryElements addObject:[requestElement toDictionary]];
		}
		dictionary[@"request"] = dictionaryElements;
	}
     */
	if(self.weather != nil){
		NSMutableArray * dictionaryElements = [NSMutableArray array];
		for(Weather * weatherElement in self.weather){
			[dictionaryElements addObject:[weatherElement toDictionary]];
		}
		dictionary[@"weather"] = dictionaryElements;
	}
	return dictionary;

}
@end