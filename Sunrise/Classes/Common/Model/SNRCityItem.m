//
//  SNRCityItem.m
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/21/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import "SNRCityItem.h"

@implementation SNRCityItem

+ (instancetype)cityWithName:(NSString *)name area:(NSString *)area{

    SNRCityItem *cityItem = [[self alloc] init];

    cityItem.name = name;
    cityItem.areaName = area;

    return cityItem;
}

@end
