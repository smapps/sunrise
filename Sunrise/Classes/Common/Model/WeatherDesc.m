//
//	WeatherDesc.m
//
//	Create by sameh mabrouk on 21/1/2016
//	Copyright © 2016. All rights reserved.
//



#import "WeatherDesc.h"

@interface WeatherDesc ()
@end
@implementation WeatherDesc




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	self.value = dictionary[@"value"];

	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.value != nil){
		dictionary[@"value"] = self.value;
	}
	return dictionary;

}
@end