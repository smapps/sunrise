//
//  SNRAPIClient.h
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/23/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SNRWeatherData.h"

//  Block Definition
typedef void (^SNRWeatherDataDownloadCompletion)(SNRWeatherData *data, NSError *error);

/**
 SNRAPIClient is a singleton object that queries the worldweatheronline Weather API and downloads weather
 data for a given location.
 */

@interface SNRAPIClient : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate>


// -----
// @name Initializing SNRAPIClient
// -----

/**
 Returns a shared instance of SNRAPIClient
 @returns A shared instance of SNRAPIClient
 */
+ (SNRAPIClient *)sharedAPIClient;



// -----
// @name Using SNRAPIClient
// -----

/**
 Queries the Wunderground Weather API and downloads weather data for the given location
 @param location    Location to download weather data for
 @param tag         Tag of the weather view expecting to receive the downloaded weather data
 @param completion  Block that returns a SOLWeatherData object on success, and nil on failure
 */
- (void)dataForLocation:(CLLocation *)location withTag:(NSInteger)tag completion:(SNRWeatherDataDownloadCompletion)completion;

/**
 Queries the worldweatheronline Weather API and downloads weather data for the given location
 @param location    Location to download weather data for
 @param tag         Tag of the weather view expecting to receive the downloaded weather data
 @param completion  Block that returns a SNRWeatherData object on success, and nil on failure
 */
- (void)dataForLocation:(CLLocation *)location city:(NSString*)cityName placemark:(CLPlacemark *)placemark withTag:(NSInteger)tag completion:(SNRWeatherDataDownloadCompletion)completion;

/**
 Queries the worldweatheronline Weather API and downloads weather data for the given location
 @param placemark   Placemark to download weather data for
 @param tag         Tag of the weather view expecting to receive the downloaded weather data
 @param completion  Block that returns a SNRWeatherData object on success, and nil on failure
 */
- (void)dataForPlacemark:(CLPlacemark *)placemark withTag:(NSInteger)tag completion:(SNRWeatherDataDownloadCompletion)completion;


@end
