//
//  SNRWeatherData.h
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/23/16.
//  Copyright © 2016 smapps. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreLocation/CoreLocation.h>

typedef struct {
    CGFloat fahrenheit;
    CGFloat celsius;
} SNRTemperature;

static inline SNRTemperature SNRTemperatureMake(CGFloat fahrenheit, CGFloat celsius) {
    return (SNRTemperature){fahrenheit, celsius};
}

#pragma mark - SOLWeatherSnapshot Interface

/**
 SNRWeatherSnapshot contains weather data for a single day, it has to conform to NSCoding Protocol so we can save it locally using NSKeyedUnarchiver.
 */
@interface SNRWeatherSnapshot : NSObject <NSCoding>

// -----
// @name Properties
// -----

//  Icon representing the day's conditions
@property (strong, nonatomic) NSString      *icon;

//  Name of the day of week
@property (strong, nonatomic) NSString      *dayOfWeek;

//  Description of the day's conditions
@property (strong, nonatomic) NSString      *conditionDescription;

//  Day's current temperature, if applicable
@property (assign, nonatomic) SNRTemperature currentTemperature;

//  Day's high temperature
@property (assign, nonatomic) SNRTemperature highTemperature;

//  Day's low temperature
@property (assign, nonatomic) SNRTemperature lowTemperature;

@end


#pragma mark - SOLWeatherData Interface

/**
 SOLWeatherData contains comprehensive multi-day weather data for a given location, it has to conform to NSCoding Protocol so we can save it locally using NSKeyedUnarchiver
 */
@interface SNRWeatherData : NSObject <NSCoding>

// -----
// @name Properties
// -----

//  Location whose weather data is being represented
@property (strong, nonatomic) CLPlacemark            *placemark;

//  Snapshot of the current conditions
@property (strong, nonatomic) SNRWeatherSnapshot    *currentSnapshot;

//  Snapshots of forecasted conditions
@property (strong, nonatomic) NSMutableArray        *forecastSnapshots;

//  Time when this object was created
@property (strong, nonatomic) NSDate                *timestamp;

@end