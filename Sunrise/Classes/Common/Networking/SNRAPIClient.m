//
//  SNRAPIClient.m
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/23/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import "SNRAPIClient.h"
#import "NSString+Substring.h"
#import "Climacons.h"

#import "Data.h"
#import "CurrentCondition.h"
#import "WeatherDesc.h"
#import "RootClass.h"
#import "Weather.h"

#pragma mark - SOLWundergroundDownloader Class Extension

@interface SNRAPIClient()

//  Used by SNRAPIClient to determine the names of locations based on coordinates
@property (nonatomic) CLGeocoder *geocoder;

//  API key
@property (nonatomic) NSString *key;

@end

@implementation SNRAPIClient

#pragma mark Initializing a SOLWundergroundDownloader

+ (SNRAPIClient *)sharedAPIClient
{
    static SNRAPIClient *sharedAPIClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *path = [[NSBundle mainBundle]pathForResource:@"API_KEY" ofType:@""];
        NSString *content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
        NSString *apiKey = [content stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
        NSLog(@"APIKey %@", apiKey);
        sharedAPIClient = [[SNRAPIClient alloc]initWithAPIKey:apiKey];
    });
    return sharedAPIClient;
}

- (instancetype)initWithAPIKey:(NSString *)key
{
    if(self = [super init]) {
        self.key = key;
        self.geocoder = [[CLGeocoder alloc]init];
    }
    return self;
}


#pragma mark Using a SOLWundergroundDownloader

- (void)dataForLocation:(CLLocation *)location city:(NSString*)cityName placemark:(CLPlacemark *)placemark withTag:(NSInteger)tag completion:(SNRWeatherDataDownloadCompletion)completion
{
    /*
     //  Requests are not made if the (location and completion) or the delegate is nil
     if(!cityName || !completion) {
     return;
     }
     */

    //  Turn on the network activity indicator in the status bar
    //  [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];

    //  Get the url request
    NSURLRequest *request;
    if (location) {
       request  = [self urlRequestForLocation:location];
    }
    else
        request  = [self urlRequestForCity:cityName];

    //  Make an asynchronous request to the url
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:
     ^ (NSURLResponse * response, NSData *data, NSError *connectionError) {

         //  Report connection errors as download failures to the delegate
         if(connectionError) {
             completion(nil, connectionError);
         } else {

             //  Serialize the downloaded JSON document and return the weather data to the delegate
             @try {
                 NSDictionary *JSON = [self serializedData:data];
                 //                 NSLog(@"recieved JSON %@", JSON);

                 SNRWeatherData *weatherData = [self dataFromJSON:JSON];

                 if(placemark) {
                     weatherData.placemark = placemark;
                     completion(weatherData, connectionError);
                 }
                 else if (cityName){
                     completion(weatherData, connectionError);
                 }
                 else {
                     //  Reverse geocode the given location in order to get city, state, and country
                     [self.geocoder reverseGeocodeLocation:location completionHandler: ^ (NSArray *placemarks, NSError *error) {
                         if(placemarks) {
                             weatherData.placemark = [placemarks lastObject];
                             completion(weatherData, error);
                         } else if(error) {
                             completion(nil, error);
                         }
                     }];

                 }
             }

             //  Report any failures during serialization as download failures to the delegate
             @catch (NSException *exception) {
                 completion(nil, [NSError errorWithDomain:@"SOLWundergroundDownloader Internal State Error" code:-1 userInfo:nil]);
             }

             //  Always turn off the network activity indicator after requests are fulfilled
             @finally {
                 //  [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
             }
         }
     }];
}

- (void)dataForPlacemark:(CLPlacemark *)placemark withTag:(NSInteger)tag completion:(SNRWeatherDataDownloadCompletion)completion
{
    [self dataForLocation:placemark.location city:@"" placemark:placemark withTag:tag completion:completion];
}

- (void)dataForLocation:(CLLocation *)location withTag:(NSInteger)tag completion:(SNRWeatherDataDownloadCompletion)completion
{
    [self dataForLocation:location city:nil placemark:nil withTag:tag completion:completion];
}

- (NSURLRequest *)urlRequestForCity:(NSString*)cityName
{
    NSLog(@"urlRequestForLocation");

    static NSString *baseURL =  @"http://api.worldweatheronline.com/free/v2/weather.ashx?";
    NSString *parameters = [NSString stringWithFormat:@"q=%@&format=%@&num_of_days=%@&key=%@", cityName, @"json", @"5", self.key];
    //    CLLocationCoordinate2D coordinates = location.coordinate;
    NSString *requestURL = [baseURL stringByAppendingString:parameters];
    NSLog(@"requestURL %@", requestURL);

    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    NSString *encodedUrl = [requestURL stringByAddingPercentEncodingWithAllowedCharacters:set];

//    NSString* encodedUrl = [requestURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    NSLog(@"result URL %@", encodedUrl);

    NSURL *url = [NSURL URLWithString:encodedUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    return request;
}

- (NSURLRequest *)urlRequestForLocation:(CLLocation *)location
{
    NSLog(@"urlRequestForLocation");

    static NSString *baseURL =  @"http://api.worldweatheronline.com/free/v2/weather.ashx?";

    CLLocationCoordinate2D coordinates = location.coordinate;

    NSString *parameters = [NSString stringWithFormat:@"q=%f,%f&format=%@&num_of_days=%@&key=%@", coordinates.latitude, coordinates.longitude, @"json", @"5", self.key];

    NSString *requestURL = [baseURL stringByAppendingString:parameters];
    NSLog(@"requestURL %@", requestURL);

    NSURL *url = [NSURL URLWithString:requestURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    return request;
}

- (NSDictionary *)serializedData:(NSData *)data
{
    NSError *JSONSerializationError;
    NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&JSONSerializationError];
    if(JSONSerializationError) {
        [NSException raise:@"JSON Serialization Error" format:@"Failed to parse weather data"];
    }
    return JSON;
}

- (SNRWeatherData *)dataFromJSON:(NSDictionary *)JSON
{
    RootClass *rootClass = [[RootClass alloc] initWithDictionary:JSON];

    Data *weatherData = rootClass.data;

    SNRWeatherData *data = [[SNRWeatherData alloc]init];

    CurrentCondition *currentCondition = [weatherData.currentCondition objectAtIndex:0];
    WeatherDesc *weatherDesc = [currentCondition.weatherDesc objectAtIndex:0];

    Weather *weatherForcastday0 = [weatherData.weather objectAtIndex:0];
//    Weather *weatherForcastday1 = [weatherData.weather objectAtIndex:1];
//    Weather *weatherForcastday2 = [weatherData.weather objectAtIndex:2];
//    Weather *weatherForcastday3 = [weatherData.weather objectAtIndex:3];
//    Weather *weatherForcastday4 = [weatherData.weather objectAtIndex:4];

    CGFloat currentTemperatureF = [currentCondition.tempF doubleValue];
    CGFloat currentTemperatureC = [currentCondition.tempC doubleValue];
    CGFloat currentHighTemperatureF = [weatherForcastday0.maxtempF doubleValue];
    CGFloat currentHighTemperatureC = [weatherForcastday0.maxtempC doubleValue];
    CGFloat currentLowTemperatureF = [weatherForcastday0.mintempF doubleValue];
    CGFloat currentLowTemperatureC = [weatherForcastday0.mintempC doubleValue];


    data.currentSnapshot.highTemperature = SNRTemperatureMake(currentHighTemperatureF, currentHighTemperatureC);
    data.currentSnapshot.lowTemperature = SNRTemperatureMake(currentLowTemperatureF, currentLowTemperatureC);

    data.currentSnapshot.conditionDescription   = weatherDesc.value;
    data.currentSnapshot.currentTemperature = SNRTemperatureMake(currentTemperatureF, currentTemperatureC);

    data.currentSnapshot.icon = [self iconForCondition:data.currentSnapshot.conditionDescription];

    NSLog(@"currentSnapshot %@", data.currentSnapshot.conditionDescription);
    NSLog(@"currentSnapshot icon %@", data.currentSnapshot.icon);


    // 5 day forcasting

    Weather *weatherForcastday;
    SNRWeatherSnapshot *forcastSnapshot;

    CGFloat forcastOneHighTemperatureF;
    CGFloat forcastOneHighTemperatureC;
    CGFloat forcastOneLowTemperatureF;
    CGFloat forcastOneLowTemperatureC;

    for (int i =0; i < weatherData.weather.count; i++) {

        weatherForcastday = [weatherData.weather objectAtIndex:i];
        forcastOneHighTemperatureF = [weatherForcastday.maxtempF doubleValue];
        forcastOneHighTemperatureC = [weatherForcastday.maxtempC doubleValue];
        forcastOneLowTemperatureF = [weatherForcastday.mintempF doubleValue];
        forcastOneLowTemperatureC = [weatherForcastday.mintempC doubleValue];

        forcastSnapshot = [[SNRWeatherSnapshot alloc]init];
        forcastSnapshot.highTemperature = SNRTemperatureMake(forcastOneHighTemperatureF, forcastOneHighTemperatureC);
        forcastSnapshot.lowTemperature = SNRTemperatureMake(forcastOneLowTemperatureF, forcastOneLowTemperatureC);

        forcastSnapshot.dayOfWeek = [self weekdayFromDate:weatherForcastday.date];

        [data.forecastSnapshots addObject:forcastSnapshot];
    }

    data.timestamp = [NSDate date];
    NSLog(@"TimeStamp icon %@", data.timestamp);
    NSLog(@"forecastSnapshots %@", data.forecastSnapshots);

    /*
     SNRWeatherSnapshot *forcastOne = [[SNRWeatherSnapshot alloc]init];
     CGFloat forcastOneHighTemperatureF = [weatherForcastday0.maxtempF doubleValue];
     CGFloat forcastOneHighTemperatureC = [weatherForcastday0.maxtempC doubleValue];
     CGFloat forcastOneLowTemperatureF = [weatherForcastday0.mintempF doubleValue];
     CGFloat forcastOneLowTemperatureC = [weatherForcastday0.mintempC doubleValue];

     forcastOne.highTemperature = SNRTemperatureMake(forcastOneHighTemperatureF, forcastOneHighTemperatureC);
     forcastOne.lowTemperature = SNRTemperatureMake(forcastOneLowTemperatureF, forcastOneLowTemperatureC);

     [data.forecastSnapshots addObject:forcastOne];

     */



    // [self weekdayFromDate:nil];

    /*
     NSArray *currentObservation                 = [JSON             objectForKey:@"current_condition"];

     NSArray *forecast                           = [JSON             objectForKey:@"forecast"];
     NSArray *simpleforecast                     = [forecast         valueForKey:@"simpleforecast"];
     NSArray *forecastday                        = [simpleforecast   valueForKey:@"forecastday"];
     NSArray *forecastday0                       = [forecastday      objectAtIndex:0];
     NSArray *forecastday1                       = [forecastday      objectAtIndex:1];
     NSArray *forecastday2                       = [forecastday      objectAtIndex:2];
     NSArray *forecastday3                       = [forecastday      objectAtIndex:3];

     SNRWeatherData *data = [[SNRWeatherData alloc]init];

     CGFloat currentHighTemperatureF             = [[[forecastday0 valueForKey:@"high"]  valueForKey:@"fahrenheit"]doubleValue];
     CGFloat currentHighTemperatureC             = [[[forecastday0 valueForKey:@"high"]  valueForKey:@"celsius"]doubleValue];
     CGFloat currentLowTemperatureF              = [[[forecastday0 valueForKey:@"low"]   valueForKey:@"fahrenheit"]doubleValue];
     CGFloat currentLowTemperatureC              = [[[forecastday0 valueForKey:@"low"]   valueForKey:@"celsius"]doubleValue];
     CGFloat currentTemperatureF                 = [[currentObservation valueForKey:@"temp_f"] doubleValue];
     CGFloat currentTemperatureC                 = [[currentObservation valueForKey:@"temp_c"] doubleValue];

     data.currentSnapshot.dayOfWeek              = [[forecastday0 valueForKey:@"date"] valueForKey:@"weekday"];
     data.currentSnapshot.conditionDescription   = [currentObservation valueForKey:@"weather"];
     data.currentSnapshot.icon                   = [self iconForCondition:data.currentSnapshot.conditionDescription];
     data.currentSnapshot.highTemperature        = SNRTemperatureMake(currentHighTemperatureF,   currentHighTemperatureC);
     data.currentSnapshot.lowTemperature         = SNRTemperatureMake(currentLowTemperatureF,    currentLowTemperatureC);
     data.currentSnapshot.currentTemperature     = SNRTemperatureMake(currentTemperatureF,       currentTemperatureC);

     SNRWeatherSnapshot *forecastOne             = [[SNRWeatherSnapshot alloc]init];
     forecastOne.conditionDescription            = [forecastday1 valueForKey:@"conditions"];
     forecastOne.icon                            = [self iconForCondition:forecastOne.conditionDescription];
     forecastOne.dayOfWeek                       = [[forecastday1 valueForKey:@"date"] valueForKey:@"weekday"];
     [data.forecastSnapshots addObject:forecastOne];

     SNRWeatherSnapshot *forecastTwo             = [[SNRWeatherSnapshot alloc]init];
     forecastTwo.conditionDescription            = [forecastday2 valueForKey:@"conditions"];
     forecastTwo.icon                            = [self iconForCondition:forecastTwo.conditionDescription];
     forecastTwo.dayOfWeek                       = [[forecastday2 valueForKey:@"date"] valueForKey:@"weekday"];
     [data.forecastSnapshots addObject:forecastTwo];

     SNRWeatherSnapshot *forecastThree           = [[SNRWeatherSnapshot alloc]init];
     forecastThree.conditionDescription          = [forecastday3 valueForKey:@"conditions"];
     forecastThree.icon                          = [self iconForCondition:forecastThree.conditionDescription];
     forecastThree.dayOfWeek                     = [[forecastday3 valueForKey:@"date"] valueForKey:@"weekday"];
     [data.forecastSnapshots addObject:forecastThree];

     data.timestamp = [NSDate date];
     */
    return data;
}

- (NSString *)iconForCondition:(NSString *)condition
{
    NSString *iconName = [NSString stringWithFormat:@"%c", ClimaconSun];
    NSString *lowercaseCondition = [condition lowercaseString];

    if([lowercaseCondition contains:@"clear"]) {
        iconName = [NSString stringWithFormat:@"%c", ClimaconSun];
    } else if([lowercaseCondition contains:@"cloud"]) {
        iconName = [NSString stringWithFormat:@"%c", ClimaconCloud];
    } else if([lowercaseCondition contains:@"drizzle"]  ||
              [lowercaseCondition contains:@"rain"]     ||
              [lowercaseCondition contains:@"thunderstorm"]) {
        iconName = [NSString stringWithFormat:@"%c", ClimaconRain];
    } else if([lowercaseCondition contains:@"snow"]     ||
              [lowercaseCondition contains:@"hail"]     ||
              [lowercaseCondition contains:@"ice"]) {
        iconName = [NSString stringWithFormat:@"%c", ClimaconSnow];
    } else if([lowercaseCondition contains:@"fog"]      ||
              [lowercaseCondition contains:@"overcast"] ||
              [lowercaseCondition contains:@"smoke"]    ||
              [lowercaseCondition contains:@"dust"]     ||
              [lowercaseCondition contains:@"ash"]      ||
              [lowercaseCondition contains:@"mist"]     ||
              [lowercaseCondition contains:@"haze"]     ||
              [lowercaseCondition contains:@"spray"]    ||
              [lowercaseCondition contains:@"squall"]) {
        iconName = [NSString stringWithFormat:@"%c", ClimaconHaze];
    }
    return iconName;
}

-(NSString*)weekdayFromDate:(NSString *)date
{
    //Convert NSString to NSDate
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];

    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:date];
    
    //    NSDate *now = [NSDate date];
    NSDateFormatter *weekday = [[NSDateFormatter alloc] init];
    [weekday setDateFormat: @"EEEE"];
    NSLog(@"The day of the week is: %@", [weekday stringFromDate:dateFromString]);
    
    return [weekday stringFromDate:dateFromString];
    
}

@end
