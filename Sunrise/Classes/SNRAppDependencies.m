//
//  SNRAppDependencies.m
//  Sunrise
//
//  Created by Sameh Mabrouk on 1/21/16.
//  Copyright © 2016 smapps. All rights reserved.
//

#import "SNRAppDependencies.h"

#import "SNRListWireframe.h"
#import "SNRListInteractor.h"
#import "SNRRootWireframe.h"

@interface SNRAppDependencies()

@property (nonatomic, strong) SNRListWireframe *listWireframe;

@end


@implementation SNRAppDependencies

- (id)init
{
    if ((self = [super init]))
    {
        [self configureDependencies];
    }

    return self;
}


- (void)installRootViewControllerIntoWindow:(UIWindow *)window
{
    [self.listWireframe presentListInterfaceFromWindow:window];
}


- (void)configureDependencies
{

    // List Modules Classes
    SNRListWireframe *listWireframe = [[SNRListWireframe alloc] init];
    SNRRootWireframe *rootWireframe = [[SNRRootWireframe alloc] init];

    listWireframe.rootWireframe = rootWireframe;

    self.listWireframe = listWireframe;


}

@end
